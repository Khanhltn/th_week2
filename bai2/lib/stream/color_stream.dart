import 'package:flutter/material.dart';
import 'dart:convert';

class ColorStream {
  // Stream colorStream;

  Stream<String> getColors() async* {
    final List<String> colors = [
      'cat',
      'elephant',
      'rabbit',
      'whale',
      'dragonfly',
      'mouse',
      'lion'
    ];

    yield* Stream.periodic(Duration(seconds: 1), (int t) {
      int index = t % 5;
      return colors[index];
    });
  }
}
