import 'package:flutter/material.dart';
import '../stream/color_stream.dart';
import 'dart:convert';

class App extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return HomePageState();
  }
}

class HomePageState extends State<StatefulWidget> {
  String bgText = 'dog';
  ColorStream colorStream = ColorStream();

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Stream Color',
      home: Scaffold(
        appBar: AppBar(
          title: Text('Bài 2'),
        ),
        body: Text(
          bgText,
          textAlign: TextAlign.center,
          style: const TextStyle(color: Colors.black),
        ),
        floatingActionButton: FloatingActionButton(
          child: Text('C'),
          onPressed: () {
            changeColor();
          },
        ),
      ),
    );
  }

  changeColor() async {
    colorStream.getColors().listen((eventString) {
      setState(() {
        print(eventString);
        bgText = eventString;
      });
    });
  }
}
